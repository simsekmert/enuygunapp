package com.enuygun.enuygunapp;

import android.app.Application;

import com.enuygun.enuygunapp.di.component.AppComponent;
import com.enuygun.enuygunapp.di.component.DaggerAppComponent;
import com.enuygun.enuygunapp.di.module.AppModule;

/**
 * Created by mertsimsek on 11/02/16.
 */
public class EnUygunApp extends Application {

    AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        initializeInjector();
    }

    public void initializeInjector(){
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    public AppComponent getAppComponent(){
        return appComponent;
    }
}
