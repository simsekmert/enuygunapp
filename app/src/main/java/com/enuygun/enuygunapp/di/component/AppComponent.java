package com.enuygun.enuygunapp.di.component;

import com.enuygun.enuygunapp.di.module.AppModule;
import com.enuygun.enuygunapp.domain.SearchFlightUsecase;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by mertsimsek on 11/02/16.
 */
@Singleton
@Component(modules = AppModule.class)
public interface AppComponent {

    SearchFlightUsecase searchFlightUsecase();
}
