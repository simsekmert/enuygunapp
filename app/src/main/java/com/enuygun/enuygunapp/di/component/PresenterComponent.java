package com.enuygun.enuygunapp.di.component;

import com.enuygun.enuygunapp.di.module.PresenterModule;
import com.enuygun.enuygunapp.di.scope.Fragment;
import com.enuygun.enuygunapp.ui.fragment.SearchFlightFragment;

import dagger.Component;

/**
 * Created by mertsimsek on 11/02/16.
 */
@Fragment
@Component(dependencies = AppComponent.class, modules = PresenterModule.class)
public interface PresenterComponent {

    void inject(SearchFlightFragment searchFlightFragment);
}
