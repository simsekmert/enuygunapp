package com.enuygun.enuygunapp.di.module;

import android.content.Context;

import com.enuygun.enuygunapp.EnUygunApp;
import com.enuygun.enuygunapp.domain.SearchFlightUsecase;
import com.enuygun.enuygunapp.domain.SearchFlightUsecaseImpl;
import com.enuygun.enuygunapp.model.rest.ApiSource;
import com.enuygun.enuygunapp.model.rest.IApiSoure;
import com.enuygun.enuygunapp.model.rest.retrofit.LocalResponseInterceptor;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by mertsimsek on 11/02/16.
 */
@Module
public class AppModule {

    EnUygunApp app;

    public AppModule(EnUygunApp app) {
        this.app = app;
    }

    @Provides
    @Singleton
    public Context provideApplicationContext(){
        return app;
    }

    @Provides
    @Singleton
    public Interceptor provideLocalResponseInterceptor(Context context){
        return new LocalResponseInterceptor(context);
    }

    @Provides
    @Singleton
    public OkHttpClient provideHttpClient(Interceptor interceptor){
        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.interceptors().add(interceptor);
        return okHttpClient;
    }

    @Provides
    @Singleton
    public IApiSoure provideApiSource(OkHttpClient okHttpClient){
        return new ApiSource(okHttpClient);
    }

    @Provides
    @Singleton
    public SearchFlightUsecase provideSearchFlightUsecase(IApiSoure apiSoure){
        return new SearchFlightUsecaseImpl(apiSoure);
    }
}
