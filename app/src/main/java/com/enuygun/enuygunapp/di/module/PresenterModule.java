package com.enuygun.enuygunapp.di.module;

import com.enuygun.enuygunapp.di.scope.Fragment;
import com.enuygun.enuygunapp.domain.SearchFlightUsecase;
import com.enuygun.enuygunapp.mvp.presenter.SearchFlightPresenter;
import com.enuygun.enuygunapp.mvp.presenter.SearchFlightPresenterImpl;

import dagger.Module;
import dagger.Provides;

/**
 * Created by mertsimsek on 11/02/16.
 */
@Module
public class PresenterModule {

    @Provides
    @Fragment
    public SearchFlightPresenter provideSearchFlightPresenter(SearchFlightUsecase searchFlightUsecase){
        return new SearchFlightPresenterImpl(searchFlightUsecase);
    }
}
