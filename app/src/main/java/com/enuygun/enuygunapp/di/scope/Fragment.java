package com.enuygun.enuygunapp.di.scope;

import java.lang.annotation.Retention;

import javax.inject.Scope;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by mertsimsek on 11/02/16.
 */
@Scope
@Retention(RUNTIME)
public @interface Fragment {}