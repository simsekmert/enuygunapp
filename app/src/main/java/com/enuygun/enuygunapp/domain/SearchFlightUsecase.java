package com.enuygun.enuygunapp.domain;

import com.enuygun.enuygunapp.ui.adapter.FlightInfoWrapper;

import rx.Observable;

/**
 * Created by mertsimsek on 11/02/16.
 */
public interface SearchFlightUsecase extends Usecase{

    Observable<FlightInfoWrapper> searchFlight();
}
