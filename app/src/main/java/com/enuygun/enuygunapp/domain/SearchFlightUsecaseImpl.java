package com.enuygun.enuygunapp.domain;

import android.util.Log;

import com.enuygun.enuygunapp.model.entity.AirlineWrapper;
import com.enuygun.enuygunapp.model.entity.AirportWrapper;
import com.enuygun.enuygunapp.model.entity.Departure;
import com.enuygun.enuygunapp.model.rest.IApiSoure;
import com.enuygun.enuygunapp.ui.adapter.FlightInfo;
import com.enuygun.enuygunapp.ui.adapter.FlightInfoDetail;
import com.enuygun.enuygunapp.ui.adapter.FlightInfoWrapper;

import javax.inject.Inject;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by mertsimsek on 11/02/16.
 */
public class SearchFlightUsecaseImpl implements SearchFlightUsecase {

    IApiSoure apiSoure;

    @Inject
    public SearchFlightUsecaseImpl(IApiSoure apiSoure) {
        this.apiSoure = apiSoure;
    }

    @Override
    public Observable<FlightInfoWrapper> searchFlight() {
        return apiSoure.searchFlight()
                .map(searchFlightResponse -> {
                    FlightInfoWrapper flightInfoWrapper = new FlightInfoWrapper();
                    AirlineWrapper airlineWrapper = searchFlightResponse.airlineWrapper;
                    AirportWrapper airportWrapper = searchFlightResponse.airportWrapper;

                    FlightInfo flightInfo;
                    FlightInfoDetail flightInfoDetail;

                    for (Departure departure : searchFlightResponse.departureWrapper.departures) {
                        flightInfo = new FlightInfo();
                        flightInfoDetail = new FlightInfoDetail();

                        //Flight infos
                        String airlineKey = departure.segments.get(0).marketingAirline;
                        flightInfo.airlineName = airlineWrapper.airlines.get(airlineKey).name;
                        flightInfo.departureTime = departure.segments.get(0).departureDatetime.time;
                        flightInfo.arrivalTime = departure.segments.get(0).arrivalDatetime.time;
                        flightInfo.price = departure.price;
                        flightInfo.currency = departure.priceCurrency;
                        String airportKeyOrigin = departure.segments.get(0).origin;
                        String airportKeyDestination = departure.segments.get(0).destination;
                        flightInfo.originCode = airportKeyOrigin;
                        flightInfo.destinationCode = airportKeyDestination;
                        flightInfo.destinationCity = airportWrapper.airports.get(airportKeyDestination).cityName;
                        flightInfo.originCity = airportWrapper.airports.get(airportKeyOrigin).cityName;

                        Log.v("TEST","City" + flightInfo.originCity);
                        Log.v("TEST","City2" + flightInfo.destinationCity);

                        //Flight info details
                        flightInfoDetail.flightClass = departure.segments.get(0).segmentClass;
                        flightInfoDetail.flightNumber = departure.segments.get(0).flightNumber;
                        flightInfoDetail.marketing = departure.segments.get(0).marketingAirline;
                        flightInfoDetail.departureInfo = departure.segments.get(0).departureDatetime.date
                                + " " + flightInfo.departureTime + " - " + flightInfo.originCode
                                + " " + airportWrapper.airports.get(airportKeyOrigin).airportName;
                        flightInfoDetail.arrivalInfo = departure.segments.get(0).arrivalDatetime.date
                                + " " + flightInfo.arrivalTime + " - " + flightInfo.destinationCode
                                + " " + airportWrapper.airports.get(airportKeyDestination).airportName;
                        flightInfo.flightInfoDetail.add(flightInfoDetail);
                        flightInfoWrapper.flightInfos.add(flightInfo);
                    }

                    return flightInfoWrapper;
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

}
