package com.enuygun.enuygunapp.model.entity;

import java.util.HashMap;

/**
 * Created by mertsimsek on 11/02/16.
 */
public class AirlineWrapper {
    public HashMap<String,Airline> airlines;

    public AirlineWrapper() {
        airlines = new HashMap<>();
    }
}
