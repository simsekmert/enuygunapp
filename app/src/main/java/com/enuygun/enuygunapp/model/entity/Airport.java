package com.enuygun.enuygunapp.model.entity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mertsimsek on 11/02/16.
 */
public class Airport {
    @SerializedName("airport_code")
    public String airportCode;

    @SerializedName("slug")
    public String slug;

    @SerializedName("airport_name")
    public String airportName;

    @SerializedName("city_code")
    public String cityCode;

    @SerializedName("city_name")
    public String cityName;

    @SerializedName("country_code")
    public String countryCode;

    @SerializedName("country_name")
    public String countryName;

    @SerializedName("latitude")
    public String latitude;

    @SerializedName("langitude")
    public String langitude;
}
