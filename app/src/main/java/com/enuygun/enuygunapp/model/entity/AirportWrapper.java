package com.enuygun.enuygunapp.model.entity;

import java.util.HashMap;

/**
 * Created by mertsimsek on 11/02/16.
 */
public class AirportWrapper {
    public HashMap<String, Airport> airports;

    public AirportWrapper() {
        airports = new HashMap<>();
    }
}
