package com.enuygun.enuygunapp.model.entity;

import java.math.BigInteger;

/**
 * Created by mertsimsek on 11/02/16.
 */
public class DateTime {
    public String date;
    public String time;
    public BigInteger timeStamp;
}
