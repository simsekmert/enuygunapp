package com.enuygun.enuygunapp.model.entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mertsimsek on 11/02/16.
 */
public class Departure {

    @SerializedName("enuid")
    public String enuid;

    @SerializedName("provider")
    public String provider;

    @SerializedName("price")
    public double price;

    @SerializedName("price_currency")
    public String priceCurrency;

    @SerializedName("price_details")
    public PriceDetail priceDetail;

    @SerializedName("infos")
    public Info info;

    @SerializedName("segments")
    public List<Segment> segments;


}
