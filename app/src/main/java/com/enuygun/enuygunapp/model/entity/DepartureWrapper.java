package com.enuygun.enuygunapp.model.entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mertsimsek on 11/02/16.
 */
public class DepartureWrapper {

    @SerializedName("departure")
    public List<Departure> departures;
}
