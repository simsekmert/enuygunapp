package com.enuygun.enuygunapp.model.entity;

/**
 * Created by mertsimsek on 11/02/16.
 */
public class Duration {
    public int day;
    public int hour;
    public int minute;
}
