package com.enuygun.enuygunapp.model.entity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mertsimsek on 11/02/16.
 */
public class Info {
    @SerializedName("is_refundable")
    public boolean isRefundable;

    @SerializedName("is_reservable")
    public boolean isReservable;

    @SerializedName("is_promo")
    public boolean isPromo;

    @SerializedName("is_sponsored")
    public boolean isSponsored;

    @SerializedName("recommended_arrival")
    public DateTime recommendedArrival;

    @SerializedName("duration")
    public Duration duration;
}
