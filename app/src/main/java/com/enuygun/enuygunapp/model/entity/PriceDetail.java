package com.enuygun.enuygunapp.model.entity;

/**
 * Created by mertsimsek on 11/02/16.
 */
public class PriceDetail {
    public double base;
    public double service;
    public double tax;
    public double total;
    public String currency;
}
