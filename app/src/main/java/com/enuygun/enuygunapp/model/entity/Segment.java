package com.enuygun.enuygunapp.model.entity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mertsimsek on 11/02/16.
 */
public class Segment {
    @SerializedName("departure_datetime")
    public DateTime departureDatetime;

    @SerializedName("arrival_datetime")
    public DateTime arrivalDatetime;

    @SerializedName("class")
    public String segmentClass;

    @SerializedName("flight_number")
    public String flightNumber;

    @SerializedName("origin")
    public String origin;

    @SerializedName("destination")
    public String destination;

    @SerializedName("marketing_airline")
    public String marketingAirline;

    @SerializedName("operating_airline")
    public String operatingAirline;

    @SerializedName("available_seats")
    public String availableSeats;

    @SerializedName("segment_delay")
    public String segmentDelay;
}
