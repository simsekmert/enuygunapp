package com.enuygun.enuygunapp.model.entity.response;

import com.enuygun.enuygunapp.model.entity.AirlineWrapper;
import com.enuygun.enuygunapp.model.entity.AirportWrapper;
import com.enuygun.enuygunapp.model.entity.DepartureWrapper;

/**
 * Created by mertsimsek on 11/02/16.
 */
public class SearchFlightResponse {
    public AirlineWrapper airlineWrapper;
    public AirportWrapper airportWrapper;
    public DepartureWrapper departureWrapper;
}
