package com.enuygun.enuygunapp.model.rest;

import com.enuygun.enuygunapp.model.entity.response.SearchFlightResponse;
import com.enuygun.enuygunapp.model.rest.retrofit.RestInterface;
import com.enuygun.enuygunapp.model.rest.retrofit.SearchFlightDeserializer;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;

import javax.inject.Inject;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;
import rx.Observable;

/**
 * Created by mertsimsek on 11/02/16.
 */
public class ApiSource implements IApiSoure {

    RestInterface restInterface;

    @Inject
    public ApiSource(OkHttpClient okHttpClient) {

        Gson gsonBuilder = new GsonBuilder()
                .registerTypeAdapter(SearchFlightResponse.class, new SearchFlightDeserializer())
                .create();

        Retrofit radioRestAdapter = new Retrofit.Builder()
                .baseUrl("http://enuygun.com/service/")
                .addConverterFactory(GsonConverterFactory.create(gsonBuilder))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(okHttpClient)
                .build();

        restInterface = radioRestAdapter.create(RestInterface.class);
    }

    @Override
    public Observable<SearchFlightResponse> searchFlight() {
        return restInterface.searchFlight();
    }
}
