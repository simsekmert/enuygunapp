package com.enuygun.enuygunapp.model.rest;

import com.enuygun.enuygunapp.model.entity.response.SearchFlightResponse;

import rx.Observable;

/**
 * Created by mertsimsek on 11/02/16.
 */
public interface IApiSoure {

    Observable<SearchFlightResponse> searchFlight();
}
