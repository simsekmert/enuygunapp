package com.enuygun.enuygunapp.model.rest.retrofit;

import android.content.Context;
import android.content.res.Resources;

import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.Protocol;
import com.squareup.okhttp.Response;
import com.squareup.okhttp.ResponseBody;

import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;

import javax.inject.Inject;

import okio.Buffer;

/**
 * Created by mertsimsek on 11/02/16.
 */
public class LocalResponseInterceptor implements Interceptor {

    Context context;
    Resources resources;

    @Inject
    public LocalResponseInterceptor(Context context) {
        this.context = context;
        resources = context.getResources();
    }

    @Override
    public Response intercept(Chain chain) throws IOException {

        String fileName = "search_results";
        fileName = fileName.toLowerCase();

        int resourceId = resources.getIdentifier(fileName, "raw",context.getPackageName());

        if (resourceId == 0) {
            throw new IOException("Could not find res/raw/" + fileName + ".json");
        }

        InputStream inputStream = resources.openRawResource(resourceId);

        String mimeType = URLConnection.guessContentTypeFromStream(inputStream);
        if (mimeType == null) {
            mimeType = "application/json";
        }

        Buffer input = new Buffer().readFrom(inputStream);

        return new Response.Builder()
                .request(chain.request())
                .protocol(Protocol.HTTP_1_1)
                .code(200)
                .body(ResponseBody.create(MediaType.parse(mimeType), input.size(), input))
                .build();
    }
}
