package com.enuygun.enuygunapp.model.rest.retrofit;

import com.enuygun.enuygunapp.model.entity.response.SearchFlightResponse;

import retrofit.http.GET;
import rx.Observable;

/**
 * Created by mertsimsek on 11/02/16.
 */
public interface RestInterface {

    @GET("searchFlight")
    Observable<SearchFlightResponse> searchFlight();
}
