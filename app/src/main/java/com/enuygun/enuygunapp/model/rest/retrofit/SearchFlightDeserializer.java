package com.enuygun.enuygunapp.model.rest.retrofit;


import com.enuygun.enuygunapp.model.entity.Airline;
import com.enuygun.enuygunapp.model.entity.AirlineWrapper;
import com.enuygun.enuygunapp.model.entity.Airport;
import com.enuygun.enuygunapp.model.entity.AirportWrapper;
import com.enuygun.enuygunapp.model.entity.DepartureWrapper;
import com.enuygun.enuygunapp.model.entity.response.SearchFlightResponse;
import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.Iterator;

/**
 * Created by mertsimsek on 11/02/16.
 */
public class SearchFlightDeserializer implements JsonDeserializer<SearchFlightResponse> {

    private static final String CONSTANT_ARRAY_AIRLINES = "airlines";
    private static final String CONSTANT_ARRAY_AIRPORTS = "airports";
    private static final String CONSTANT_ARRAY_FLIGHTS = "flights";


    @Override
    public SearchFlightResponse deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        SearchFlightResponse searchFlightResponse = new SearchFlightResponse();
        Gson gson = new Gson();

        try {
            JSONObject jsonResult = new JSONObject(json.toString());

            /**
             * Parse Airlines
             */
            AirlineWrapper airlineWrapper = new AirlineWrapper();
            JSONObject jsonAirlines = jsonResult.getJSONObject(CONSTANT_ARRAY_AIRLINES);

            Iterator airlinesIterator = jsonAirlines.keys();
            while (airlinesIterator.hasNext()){
                String key = (String) airlinesIterator.next();
                Airline airline = gson.fromJson(jsonAirlines.get(key).toString(), Airline.class);
                airlineWrapper.airlines.put(key, airline);
            }

            searchFlightResponse.airlineWrapper = airlineWrapper;

            /**
             * Parse Airports
             */
            AirportWrapper airportWrapper = new AirportWrapper();
            JSONObject jsonAirports = jsonResult.getJSONObject(CONSTANT_ARRAY_AIRPORTS);

            Iterator airportsIterator = jsonAirports.keys();
            while (airportsIterator.hasNext()){
                String key = (String) airportsIterator.next();
                Airport airport = gson.fromJson(jsonAirports.get(key).toString(), Airport.class);
                airportWrapper.airports.put(key, airport);
            }

            searchFlightResponse.airportWrapper = airportWrapper;

            /**
             * Parse Flights
             */
            JSONObject jsonDepartures = jsonResult.getJSONObject(CONSTANT_ARRAY_FLIGHTS);
            searchFlightResponse.departureWrapper = gson.fromJson(jsonDepartures.toString(),DepartureWrapper.class);


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return searchFlightResponse;
    }
}
