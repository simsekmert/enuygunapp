package com.enuygun.enuygunapp.mvp.presenter;

import com.enuygun.enuygunapp.mvp.view.MVPView;

/**
 * Created by mertsimsek on 11/02/16.
 */
public interface MVPPresenter {

    void attachView(MVPView mvpView);

    void start();

    void stop();
}
