package com.enuygun.enuygunapp.mvp.presenter;

/**
 * Created by mertsimsek on 11/02/16.
 */
public interface SearchFlightPresenter extends MVPPresenter{

    void searchFlight();

}
