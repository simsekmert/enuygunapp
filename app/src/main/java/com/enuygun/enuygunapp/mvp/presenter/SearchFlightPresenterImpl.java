package com.enuygun.enuygunapp.mvp.presenter;

import com.enuygun.enuygunapp.domain.SearchFlightUsecase;
import com.enuygun.enuygunapp.mvp.view.MVPView;
import com.enuygun.enuygunapp.mvp.view.SearchFlightView;

import javax.inject.Inject;

import rx.Subscription;

/**
 * Created by mertsimsek on 11/02/16.
 */
public class SearchFlightPresenterImpl implements SearchFlightPresenter{

    SearchFlightUsecase searchFlightUsecase;
    SearchFlightView searchFlightView;
    Subscription subscription;

    @Inject
    public SearchFlightPresenterImpl(SearchFlightUsecase searchFlightUsecase) {
        this.searchFlightUsecase = searchFlightUsecase;
    }

    @Override
    public void searchFlight() {
        searchFlightView.showProgress();
         subscription = searchFlightUsecase.searchFlight()
                .subscribe(searchFlightResponse -> {
                    searchFlightView.onFlightsLoaded(searchFlightResponse);
                    searchFlightView.dismissProgress();
                });
    }

    @Override
    public void attachView(MVPView mvpView) {
        searchFlightView = (SearchFlightView) mvpView;
    }

    @Override
    public void start() {

    }

    @Override
    public void stop() {

    }
}
