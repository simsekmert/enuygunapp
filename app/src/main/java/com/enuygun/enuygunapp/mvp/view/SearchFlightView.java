package com.enuygun.enuygunapp.mvp.view;

import com.enuygun.enuygunapp.ui.adapter.FlightInfoWrapper;

/**
 * Created by mertsimsek on 11/02/16.
 */
public interface SearchFlightView extends MVPView{

    void showProgress();

    void onFlightsLoaded(FlightInfoWrapper flightInfoWrapper);

    void dismissProgress();
}
