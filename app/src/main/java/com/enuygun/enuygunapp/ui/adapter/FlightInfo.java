package com.enuygun.enuygunapp.ui.adapter;

import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mertsimsek on 12/02/16.
 */
public class FlightInfo implements ParentListItem{

    public String logo;
    public String airlineName;
    public String departureTime;
    public String originCode;
    public String arrivalTime;
    public String destinationCode;
    public String originCity;
    public String destinationCity;
    public double price;
    public String currency;
    public List<FlightInfoDetail> flightInfoDetail;

    public FlightInfo() {
        flightInfoDetail = new ArrayList<>();
    }

    @Override
    public List<?> getChildItemList() {
        return flightInfoDetail;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return false;
    }
}
