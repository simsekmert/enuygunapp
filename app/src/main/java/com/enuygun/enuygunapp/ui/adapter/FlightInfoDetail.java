package com.enuygun.enuygunapp.ui.adapter;

/**
 * Created by mertsimsek on 12/02/16.
 */
public class FlightInfoDetail {
    public String marketing;
    public String flightNumber;
    public String flightClass;
    public String departureInfo;
    public String arrivalInfo;
}
