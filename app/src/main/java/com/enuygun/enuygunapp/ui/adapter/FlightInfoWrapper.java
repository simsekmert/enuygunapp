package com.enuygun.enuygunapp.ui.adapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mertsimsek on 12/02/16.
 */
public class FlightInfoWrapper {
    public List<FlightInfo> flightInfos;

    public FlightInfoWrapper() {
        flightInfos = new ArrayList<>();
    }
}
