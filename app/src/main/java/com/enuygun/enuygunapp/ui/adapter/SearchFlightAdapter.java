package com.enuygun.enuygunapp.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.Adapter.ExpandableRecyclerAdapter;
import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;
import com.bignerdranch.expandablerecyclerview.ViewHolder.ChildViewHolder;
import com.bignerdranch.expandablerecyclerview.ViewHolder.ParentViewHolder;
import com.enuygun.enuygunapp.R;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by mertsimsek on 12/02/16.
 */
public class SearchFlightAdapter extends ExpandableRecyclerAdapter<SearchFlightAdapter.FlightParentViewHolder,SearchFlightAdapter.FlightChildViewHolder> {

    private LayoutInflater inflater;

    public SearchFlightAdapter(Context context, @NonNull List<? extends ParentListItem> parentItemList) {
        super(parentItemList);
        inflater = LayoutInflater.from(context);
    }

    @Override
    public FlightParentViewHolder onCreateParentViewHolder(ViewGroup parentViewGroup) {
        View parentView = inflater.inflate(R.layout.item_flight_parent, parentViewGroup, false);
        return new FlightParentViewHolder(parentView);
    }

    @Override
    public FlightChildViewHolder onCreateChildViewHolder(ViewGroup childViewGroup) {
        View childView = inflater.inflate(R.layout.item_flight_child, childViewGroup, false);
        return new FlightChildViewHolder(childView);
    }

    @Override
    public void onBindParentViewHolder(FlightParentViewHolder parentViewHolder, int position, ParentListItem parentListItem) {
        FlightInfo flightInfo = (FlightInfo) parentListItem;
        parentViewHolder.bind(flightInfo);
    }

    @Override
    public void onBindChildViewHolder(FlightChildViewHolder childViewHolder, int position, Object childListItem) {
        FlightInfoDetail flightInfoDetail = (FlightInfoDetail) childListItem;
        childViewHolder.bind(flightInfoDetail);
    }

    public class FlightChildViewHolder extends ChildViewHolder {

        @Bind(R.id.textview_marketing)
        TextView textViewMarketing;

        @Bind(R.id.textview_flight_number)
        TextView textViewFlightNumber;

        @Bind(R.id.textview_class)
        TextView textViewClass;

        @Bind(R.id.textview_departure_info)
        TextView textViewDepartureInfo;

        @Bind(R.id.textview_arrival_info)
        TextView textViewArrivalInfo;

        Context context;

        public FlightChildViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(FlightInfoDetail flightInfoDetail){
            textViewMarketing.setText(flightInfoDetail.marketing);
            textViewClass.setText(flightInfoDetail.flightClass);
            textViewFlightNumber.setText(flightInfoDetail.flightNumber);
            textViewArrivalInfo.setText(flightInfoDetail.arrivalInfo);
            textViewDepartureInfo.setText(flightInfoDetail.departureInfo);

        }
    }

    public class FlightParentViewHolder extends ParentViewHolder {

        @Bind(R.id.imageview_provider)
        ImageView imageViewProvider;

        @Bind(R.id.textview_provider)
        TextView textViewProvider;

        @Bind(R.id.textview_departure_time)
        TextView textViewDepartureTime;

        @Bind(R.id.textview_departure_code)
        TextView textViewDepartureCode;

        @Bind(R.id.textview_arrival_time)
        TextView textViewArrivalTime;

        @Bind(R.id.textview_arrival_code)
        TextView textViewArrivalCode;

        @Bind(R.id.textview_departure_airport)
        TextView textviewDepartureAirport;

        @Bind(R.id.textview_arrival_airport)
        TextView textviewArrivaAirport;

        @Bind(R.id.textview_price)
        TextView textViewPrice;

        @Bind(R.id.textview_price_currency)
        TextView textViewCurrency;

        public FlightParentViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(FlightInfo flightInfo){
            textViewProvider.setText(flightInfo.airlineName);
            textViewDepartureTime.setText(flightInfo.departureTime);
            textViewDepartureCode.setText(flightInfo.originCode);
            textViewArrivalTime.setText(flightInfo.arrivalTime);
            textViewArrivalCode.setText(flightInfo.destinationCode);
            textviewDepartureAirport.setText(flightInfo.originCity);
            textviewArrivaAirport.setText(flightInfo.destinationCity);
            textViewPrice.setText(String.valueOf(flightInfo.price));
            textViewCurrency.setText(flightInfo.currency);
        }

    }
}
