package com.enuygun.enuygunapp.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.enuygun.enuygunapp.EnUygunApp;
import com.enuygun.enuygunapp.R;
import com.enuygun.enuygunapp.di.component.DaggerPresenterComponent;
import com.enuygun.enuygunapp.di.module.PresenterModule;
import com.enuygun.enuygunapp.mvp.presenter.SearchFlightPresenter;
import com.enuygun.enuygunapp.mvp.view.SearchFlightView;
import com.enuygun.enuygunapp.ui.adapter.FlightInfoWrapper;
import com.enuygun.enuygunapp.ui.adapter.SearchFlightAdapter;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by mertsimsek on 11/02/16.
 */
public class SearchFlightFragment extends Fragment implements SearchFlightView{

    @Bind(R.id.recyclerview_flights)
    RecyclerView recyclerViewFlights;

    @Inject
    SearchFlightPresenter searchFlightPresenter;

    SearchFlightAdapter searchFlightAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeInjector();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search_flights, container, false);
        ButterKnife.bind(this,view);
        recyclerViewFlights.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerViewFlights.setLayoutManager(linearLayoutManager);
        searchFlightPresenter.attachView(this);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        searchFlightPresenter.searchFlight();
    }

    private void initializeInjector(){
        EnUygunApp app = (EnUygunApp) getActivity().getApplication();

        DaggerPresenterComponent.builder()
                .appComponent(app.getAppComponent())
                .presenterModule(new PresenterModule())
                .build()
                .inject(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        searchFlightPresenter.stop();
    }

    @Override
    public void showProgress() {
    }

    @Override
    public void onFlightsLoaded(FlightInfoWrapper flightInfoWrapper) {
        searchFlightAdapter = new SearchFlightAdapter(getActivity().getApplicationContext(), flightInfoWrapper.flightInfos);
        recyclerViewFlights.setAdapter(searchFlightAdapter);
    }

    @Override
    public void dismissProgress() {
    }
}
